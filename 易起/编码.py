import sys,traceback,base64
from urllib import parse

def 编码_UTF8编码(内容):
    '失败返回空文本'
    try:
        return 内容.encode(encoding='UTF-8', errors='strict')
    except:
        print("{}|运行出错\r\n{}\r\n".format(sys._getframe().f_code.co_name, traceback.format_exc()))
        return ''


def 编码_UTF8解码(内容):
    '失败返回空文本'
    try:
        return 内容.decode(encoding='UTF-8', errors='strict')
    except:
        print("{}|运行出错\r\n{}\r\n".format(sys._getframe().f_code.co_name, traceback.format_exc()))
        return ''


def 编码_URL编码(内容):
    '失败返回空文本'
    try:
        return parse.quote(内容)
    except:
        print("{}|运行出错\r\n{}\r\n".format(sys._getframe().f_code.co_name, traceback.format_exc()))
        return ''


def 编码_URL解码(内容):
    '失败返回空文本'
    try:
        return parse.unquote(内容)
    except:
        print("{}|运行出错\r\n{}\r\n".format(sys._getframe().f_code.co_name, traceback.format_exc()))
        return ''


def 编码_GBK编码(内容):
    '失败返回空文本'
    try:
        return 内容.encode(encoding='GBK', errors='strict')
    except:
        print("{}|运行出错\r\n{}\r\n".format(sys._getframe().f_code.co_name, traceback.format_exc()))
        return ''


def 编码_GBK解码(内容):
    '失败返回空文本'
    try:
        return 内容.decode(encoding='GBK', errors='strict')
    except:
        print("{}|运行出错\r\n{}\r\n".format(sys._getframe().f_code.co_name, traceback.format_exc()))
        return ''


def 编码_ANSI到USC2(内容):
    '失败返回空文本'
    try:
        return ascii(内容)
    except:
        print("{}|运行出错\r\n{}\r\n".format(sys._getframe().f_code.co_name, traceback.format_exc()))
        return ''


def 编码_USC2到ANSI(内容):
    '失败返回空文本'
    try:
        return 内容.encode('utf-8').decode('unicode_escape')
    except:
        print("{}|运行出错\r\n{}\r\n".format(sys._getframe().f_code.co_name, traceback.format_exc()))
        return ''


def 编码_BASE64编码(内容):
    '失败返回空,要编码的内容支持文本跟字节集'
    try:
        if type(内容)==str:
            return base64.b64encode(内容.encode('UTF-8')).decode("UTF-8")
        else:
            return base64.b64encode(内容).decode("UTF-8")
    except:
        print("{}|运行出错\r\n{}\r\n".format(sys._getframe().f_code.co_name, traceback.format_exc()))
        return ''


def 编码_BASE64解码(内容, 返回字节集=False):
    '失败返回空文本,字节集用于解码图片'
    try:
        if 返回字节集:
            return base64.b64decode(内容)
        else:
            return base64.b64decode(内容).decode("UTF-8")
    except:
        print("{}|运行出错\r\n{}\r\n".format(sys._getframe().f_code.co_name, traceback.format_exc()))
        return ''